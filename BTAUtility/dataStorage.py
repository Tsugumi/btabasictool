import re
import os
import httplib2
from . import GetJingClass

URL_Prefix_CBETA_TXT_In_GITHUB="https://raw.githubusercontent.com/ddbc/CBETA-txt/master"
URL_Prefix_CBETA_XML_In_GITHUB="https://raw.githubusercontent.com/ddbc/CBETA_TAFxml/master"
class physicalDataStore:
	"""用來管理下載過後的資料，避免重複下載，浪費資源
	"""
	def __init__(self, dataDir="download"):
		"""初始化函式, 預設檔案位置為download/
		"""
		assert(len(dataDir)>0) 
		self.dsDir=dataDir
		self.xmlFile_rex = re.compile("^(?P<canon>T|X|S)(?P<jing>\d+[a-z]?).xml$",re.VERBOSE)
		self.textFile_rex = re.compile("^(?P<canon>T|X|S)(?P<jing>\d+[a-z]?)_(?P<juan>\d{3}).xml$",re.VERBOSE)
		
		if not os.path.exists(self.dsDir):
			os.makedirs(self.dsDir)
		
		if not os.path.exists(os.path.join(self.dsDir,"xml")):
			os.makedirs(os.path.join(self.dsDir,"xml"))
			#print("{0} 資料夾不存在 ... 協助建立".format(os.path.join(self.dsDir,"xml")))
			
		if not os.path.exists(os.path.join(self.dsDir,"txt")):
			os.makedirs(os.path.join(self.dsDir,"txt"))
			#print("{0} 資料夾不存在... 協助建立".format(os.path.join(self.dsDir,"xml")))
	
	def tUtoFileName(self,tU,type="txt"):
		"""將tU轉為應該出現的檔案名稱
			 輸入：tU, 轉換型態 (txt/xml)
			 輸出：Tuple : (local txt 檔案位置, 網路下載檔案位置, 下載後儲存位置)
			 附註：若下載後儲存位置 = None 表示使用 local txt 檔案位置
		"""
		if tU.dataType=="jing" and type=="txt":
			#回傳(local txt path,gitHub file Path, File Save Path,)
			return (os.path.join(
										self.dsDir,"txt",tU.canon,tU.jingNoStr, \
										"{0}_000.txt".format(tU.jingNoStr)
										), 
							"/".join([tU.canon,tU.jingNoStr, \
										"{0}_000.txt".format(tU.jingNoStr)]
										),
							None		#表示下載檔案直接儲存於 local txt path
						)
		elif tU.dataType=="juan" and type=="xml":
			#回傳(localpath,gitHubPath)
			return (os.path.join(  
										self.dsDir,"txt",tU.canon,tU.jingNoStr, \
										"{0}_{1:03d}.txt".format(tU.jingNoStr,tU.juanNo)
										),
							"/".join([tU.canon, \
										"{0}.xml".format(tU.jingNoStr)]
										),
							os.path.join(
										self.dsDir,"xml",tU.canon, \
										"{0}.xml".format(tU.jingNoStr)
										)
						)
		elif tU.dataType=="discourse":
			return None
		elif tU.dataType=="line":
			return None
		elif tU.dataType=="custom":
			#回傳(localpath,gitHubPath,local txt path)
			return (tU.filename,None,None)
		else:
			return None
	


	def prepareData(self, tU):
		""" 準備資料：確認與下載
			  參數：textUint 資料結構 tU
		      功能：確認資料是否已經在資料夾中，或啟動網路下載功能
		"""
		from . import textdata
		assert(isinstance(tU,textdata.textUnit))
		
		from . import textdata
		#先把設定資料轉換為應出現的檔案名稱
		if tU.dataType == "jing":
			localFilePath,iFilePath,saveFilePath=self.tUtoFileName(tU,type="txt")
		elif tU.dataType=="juan":
			localFilePath,iFilePath,saveFilePath=self.tUtoFileName(tU,type="xml")
		elif tU.dataType in ["discourse","line","custom"]:
			print ("尚未支援的TU型態:{0}".format(tU.dataType))
			exit()
		
		#localFile存在
		if os.path.exists(localFilePath):
			print("{0}: 檔案已在local".format(localFilePath))
		else:
			print("{0}: 檔案不存在local".format(localFilePath))
			if (tU.dataType == "jing"):
				
				#下載檔案，並存入localFilePath
				iFileURL=URL_Prefix_CBETA_TXT_In_GITHUB +"/"+ iFilePath
				s=self.dataDownload(iFileURL, localFilePath)
			elif (tU.dataType == "juan"):	
				
				#下載檔案，並存入saveFilePath
				iFileURL=URL_Prefix_CBETA_XML_In_GITHUB +"/"+ iFilePath
				s=self.dataDownload(iFileURL, saveFilePath)
			
			print("下載...{0}".format(iFileURL))
				
			if s>0:	#下載失敗
				print("{0} 之檔案: {1} 無法下載, \n status code: {2}".format(tU,iFileURL,s))
				print("請確定設定之經號格式是否正確與是否在程式支援範圍內")
				exit()
			
			if (tU.dataType == "juan" and iFilePath[-3:]=="xml"):
				print("由{1} 產生xml 到 \n{0}\n".format(os.path.dirname(localFilePath),saveFilePath))
				
				#勸認目標資料夾是否存在
				localDirPath=os.path.dirname(localFilePath)
				if not os.path.exists(localDirPath):
					os.makedirs(localDirPath)
					
				#產生資料切割物件
				GetJingObj=GetJingClass.ClassGetJing(		
								contentUnit="jing",							
								inputFileName=saveFilePath,			
								dstDir=localDirPath,							
								splitByJuan=True,								
								byJuanCut=True)
				#GetJingObj.printSettings()
				GetJingObj.printTexts()
		
		#已取得資料，設定local檔案位置
		tU.setLocalPath(localFilePath)
		

	def dataDownload(self, url,localPath):
		"""  處裡網路下載 
			   參數：下載網址 url, 儲存位址 localPath
			   功能：由url下載內容，儲存於localPath
			   回傳：成功:0, 失敗傳輸status
		"""
		h = httplib2.Http(".cache")
		resp, content = h.request(url,"GET")
		#沒有成功取得資料
		if (resp.status!=200):
			return resp.status
		
		#取得UTF-8內容
		str_content = content.decode('utf-8')
		
		#確認local儲存地資料夾存在
		#print(url)
		#print(localPath)
		#print(str_content)
		localDirPath=os.path.dirname(localPath)
		if not os.path.exists(localDirPath):
			os.makedirs(localDirPath)
		
		#開檔並儲存
		with open(localPath, "w",encoding="utf-8") as text_file:
			text_file.write(str_content)
		
		return 0



