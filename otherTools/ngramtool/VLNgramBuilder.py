# -*- coding: utf-8 -*-
# 
#ver 0.1 (2014.05.08 by Joey Hung) 由ngramTool v0.3.2 繼承過來，但獨立出較為高效率之VLNgramBuilder

import time

class gram:
	def __init__(self,text,count=1,docCount=1,pos=[]):
		""" 初始化函式 """
		self.text=text									# 此gram的字串
		self.count=count							# 此gram的出現總字數
		self.docCount=docCount			# 此gram的出現的文件總數
		self.pos=pos									# 此gram在各文件中出現的詳細次數
		
	def allstr(self):
		""" 列印此gram的詳細內容 """
		return "%s: %d(%d) [%s]"%(self.text,self.count,self.docCount,",".join([str(x) for x in self.pos]))
				
			
	def updateCount(self):
		""" 物件內部自動更新count 的method """
		self.count =sum(self.pos)
		self.docCount=sum([1 for x in self.pos if x>0])
		
	def __str__(self):
		""" 列近此gram 的代表字串, 不列印:此gram在各文件中出現次數陣列 """
		return "%s: %d(%d)"%(self.text,self.count,self.docCount)
	
	def __lt__(self, other):
		""" overwrite < 運算子，排序用, 讓排序變成text的字串排序 """
		return (self.text<other.text)
	
		
class VLNgram:
	"""
		資料結構紀錄：
		self.srcTextArray : 原始字串array :: [str1,str2.....,strN]
		
		本版本取消各經存放陣列。
		
		-- 各sample gram --
		共有四個結構：self.gramsDict, self.allGramsDict, self.grams, self.allGrams
		內容分成分兩種類型：
					1-b. self.allGramsDict  : 單一dictionary，代表所有文章處裡後，所有不重複gram組成的字典結構。
							= 內容: {gram1,gram2,gram3.........}
							存取方式：self.allGramsDict["如是"]
					附註：以上兩範例兩者會連結到同一份gram 實體

				2. 根據某key排序過的gram清單。(好處是可以跟據key值排序的結果，逐一取得gram與相關資訊)
					2-b. self.allGrams  : 單一List，代表所有文章，所按照 sortBy排序之後的 List。
		
	"""
	def __init__(self,textArr):
		""" 初始化VLNgram 計算物件 """
		self.srcTextArray=textArr[:]								#文字原始資料，一文檔=一item
		self.sampleSize=len(self.srcTextArray)		#文檔總數

	def __getitem__(self,item):
		"""	回傳含有全部字串之字典物件 """
		return self.allgramsDict[item]
	
	def sortedItems(self,max=None):
		"""	回傳利用全部文章內容經由排序後的gram清單 """
		return self.allGrams[:max]

		
	def textDecomp(self,s,minLen):  #s: gram text, gramMinLen
		"""	產生某一gram的所有可能子gram, 用於gram抵銷子gram次數用
				minLen 是用來設定產生下限，以便快速結束計算程序"""
		return [ s[i:i+n] for n in range(len(s)-1,minLen-1,-1) for i in range(len(s)-n+1)  ]
		
		
	def sort_dict_by_length(self,d):  #d 傳dictionary進來，傳回排完的list
		res=[g for g in d.values() ] # 由d 產生 key, value 的 tuple list
		return sorted(res,key=lambda x: len(x.text),reverse = True) 
		
	def sort_dict_by_count(self,d,index="all"):  #d 傳dictionary進來，傳回排完的list
		#index 表示要排序的是第幾個陣列, count 儲存地點不同. all 表示全部經的混合字串 
		if index =="all":
			res=[(g.count,g) for key,g in d.items() ] # 由d 產生 key, value 交換的 tuple list
		else:
			res=[(g.pos[index],g) for key,g in d.items() ] # 由d 產生 key, value 交換的 tuple list
		res.sort(reverse = True)  #排序
		res=[x[1] for x in res ] #產生 key, value 交換 res中每個 tuple的內容順序，這樣看才順眼。
		return res
	
	def sort_dict_by_key(self,d):  #d 傳dictionary進來，傳回排完的list
		res=[(g.text,g) for g in d.values() ] # 由d 產生 key, value 交換的 tuple list
		res.sort(reverse = False)  #排序
		res=[x[1] for x in res ] #產生 key, value 交換 res中每個 tuple的內容順序，這樣看才順眼。
		return res
		
		
	def sort_gramList_by_count(self,l): #d 傳List進來，傳回排完的list
		res=[(g.count,g) for g in l ] # 由l 產生 count, g 交換的 tuple list
		res.sort(reverse = True)  #排序
		res=[x[1] for x in res ] #去除排序用欄位
		return res
		
	def cleanTxt_with_gram(self,g): #傳入gram, 清理文字內容
		for i in range(len(self.srcTextArray)):
			if (g.pos[i]>0):
				self.srcTextArray[i]=self.srcTextArray[i].replace(g.text,self.internalReplaceStr*len(g.text))   #self.internalReplaceStr 預設是"@"
		
		
	def dumpTextArray(self,max=50):
		""" 將每一個文件的部份列印出來 """
		for k in range(len(self.srcTextArray)):
			print("[%d]%s...(%d words)"%(k,self.srcTextArray[k][:max],len(self.srcTextArray[k])))
		
	def dumpGrams(self,max=50):
		
		#以下針對所有文件的總和內容列印最多max gram
		print("[text all]:")
		for g in self.allGrams[:max]:
			print(g),
		print("\n")
	
	def hasAvoidStr(self,s,avoidStrs):
		""" 	判斷某一個[避免字串], 是否包含於一個字串s當中,通常使用於文件切割時
		若發現某一個字串包含s, 就回傳Flase, 主程式中將因此不建立gram物件。
		"""
		for avstr in avoidStrs:
			if avstr in s:
				return True
		return False
	
	#產生固定長度字串
	#s為一字串, n 為gram的長度，glbGramDict: 全部gram混合字典
	# simple Number 目前為第幾個樣本，為紀錄位置用
	# avoidStrs 為一個List, 內含所有要避免的Strings, ex: ["X","地獄",...]
	# glbGramDict 就是
	def gen_fl_grams(self,s,n,sampleNumber,avoidStrs):   
	
		glbGramDict= self.allgramsDict # 所有文獻總和的gramDict
		
		for i in range(len(s)-(n-1)): #固定切長度為n的字串
			bg = s[i:i+n]
			if avoidStrs and self.hasAvoidStr(bg,avoidStrs):  #avoid String 出現在字串中
				continue

			if bg not in glbGramDict:  #整體dict沒有(對所有人的第一次出現)，建立整合 gramDict --> self.allGramsDict
				arr=[0 for j in range(self.sampleSize)] #初始化這個gram資料的位置矩陣
				arr[sampleNumber]=1  #給定出現位置
				g=gram(bg,1,1,arr)	 #更新gram的總count數與doc count數
				glbGramDict[bg]=g #更新完成，加入整體dict
			else:  #整體dict有
				g=glbGramDict[bg]   #由全域dict取得gram資料
				g.count+=1	#更新gram的總count數
				if g.pos[sampleNumber]==0:		#本sample 第一次出現
					g.docCount+=1	#更新gram的doc count 值+1
					g.pos[sampleNumber]=1	#更新gram的location count 值=1
				else:
					g.pos[sampleNumber]+=1	#更新gram的location count 值=1
			
	
	
	# 建立Gram 步驟0, 初始化內部參數
	def build_gram_proc_s0_init(self): 
		self.allGrams=[]  #整合字串之 gram List
		self.allgramsDict={}  # 整合字串之 gramDict
		self.featureSet={} # 過程中所紀錄合於門檻值的grams, 最後還是會丟回self.allgramsDict
		self.internalReplaceStr="@"	#各層次過濾字串時使用的代替字串
	
	# 建立Gram 步驟N1, 呼叫self.gen_grams,切字串/計數
	# 產生各字串之 gramDict，並整合至 self.gramsDict (Global List)
	def build_gram_proc_s1_gen_fixed_len_gram(self,gramLen,verbose,avoidStrs):  
		
		for i in range(len(self.srcTextArray)):   # i, 因為要列印出目前狀態，所以需要i
			txt=self.srcTextArray[i]   # txt： 樣本的完整內容，字串型態(儲存容器在function中處裡)
			self.gen_fl_grams(txt,gramLen,i,avoidStrs)	#產生gram內容
			#if verbose and (not (i%100) or not(len(self.srcTextArray)-1-i)):  
				#print ("%i..," % i, end="")
		#if verbose:  #列印換行
			#print ("")
		
		
	def build_gram_proc_s2_docFilter_VLngram_byLayer(self,docThreshold,gramlen,verbose):  
		"""一層一層過濾文件數的方式"""
		if verbose:  
			print("正在建立/過濾[長度為%d]的gram, 文件門檻: %d, " % (gramlen,docThreshold))
		
		#所以直接針對allgramsDict進行門檻過濾
		oriSizeOfFSet=len(self.featureSet)  #先紀錄擴增前得featureSet的size
		for gstr in self.allgramsDict:
			g=self.allgramsDict[gstr]
			
			if (g.docCount>=docThreshold):						#若gram 超過門檻
				self.featureSet[gstr]=g										#加入選擇過後之dict
			else: 																				#沒有通過門檻的g，byebye
				del g																			#刪除g 物件
		
		#print(" Feature Set變化： %d => %d (新增 %d) , 本輪gram數目： %d/ 過篩比例 = %.4f %%"  % (len(self.featureSet),oriSizeOfFSet,len(self.featureSet)-oriSizeOfFSet,len(self.allgramsDict),100*(len(self.featureSet)-oriSizeOfFSet)/len(self.allgramsDict)))
		
		self.allgramsDict.clear() 											#清除allgramsDict, 以便計算下一輪，
																									#但最後會用featureSet內容補回去
	
	# 建立Gram 步驟2b , 清理文字檔
	def build_gram_proc_s2b_remove_FSgram_From_Text(self,docThreshold,gramlen,verbose):  
		"""把被選入FS的字彙，於text中除去"""
		
		# current Level Feature Set (過濾長度, 並按照使用次數排序過)
		cLFs = self.sort_gramList_by_count([ self.featureSet[gs] for gs in self.featureSet if  len(gs)==gramlen])
		
		for g in cLFs:
			#print(" -- %s: -- " % g)
			FSQualification="Yes"
			for i in range(len(self.srcTextArray)):  # 檢查gram記數是否受文字檔變化所影響
				if (g.pos[i]>0 and g.pos[i]-self.srcTextArray[i].count(g.text)>0): #gram計數有變化
					#print("  %s text-%d pos=%d, count=%d (X): " % (" -"*10, i,g.pos[i],self.srcTextArray[i].count(g.text)))
					FSQualification= "Question" #需要重新確認gram 納入Feature的資格
					g.pos[i]=self.srcTextArray[i].count(g.text)  #更新gram 於pos[i]的合理值
			
			if FSQualification== "Question":  #重新確認gram 納入Feature的資格
				g.updateCount()  #先要求gram 更新count與docCount
				#print(" Re-check %s : " % g.text, g)
				if (g.docCount<docThreshold):						#若重新計算後的gram無法超過門檻
					#print(" =====> sorry, no more %s" % g.text)
					self.featureSet.pop(g.text,"")							#將gram由featureSet移出
					del g																			#刪除g 物件
					FSQualification= "No"																			
				else:
					FSQualification= "Yes"

			if FSQualification== "Yes":
				self.cleanTxt_with_gram(g)



	# 建立Gram 步驟3 , 排序
	def build_gram_proc_s3_sort(self,sortBy,verbose):  			
		if verbose:  
			print("正在依%s排序總字串gram ..." % (sortBy))
			if sortBy=="count":
				self.allGrams=self.sort_dict_by_count(self.allgramsDict)
			else:
				self.allGrams=self.sort_dict_by_key(self.allgramsDict)
	
	
	
	def build_VLNgram(self,minGramLen,maxGramLen,sortBy,docThreshold,verbose=False,avoidStrs=None):
		""" 主入口: 用來建立可變長度的長度的ngram
			   參數：
			   1. minGramLen    (gram長度下限)
			   2. maxGramLen    (gram長度上限)
			   3. sortBy  				  (排序條件設定)
			   4. docThreshold   (VLNgram 文件門檻設定) 
			   5. verbose=False, (是否列印執行過程) 
			   6. fromFix=False,  (true: 固定長度ngram, false: 可變長度ngram) 
			   7. avoidStrs=None   (是否有避免字串) 
		"""
		# =============  初始化 Global Variables  =============
		self.build_gram_proc_s0_init() 
		# =============  檢查參數的合法性 =================
		if minGramLen<1:
			print("build_ngram時gramLength必須大於1")
			return -1
			
		if not (sortBy == "count" or sortBy == "key" ):
			print("build_ngram 的 sortby 參數必須為 count 或 key ")
			return -2
		"""
		# =============  Step1: Build Grams ==================
		self.build_gram_proc_s1_gen_gram(minGramLen,maxGramLen,verbose,avoidStrs)
		# ========  Step2: 計算文件數與門檻過濾==================
		self.build_gram_proc_s2_docFilter_VLngram(minGramLen,docThreshold,verbose)  # VLn-gram 過濾 only
		# ================  Step3: 排序 =====================
		self.build_gram_proc_s3_sort(sortBy,verbose)
		#=============================================
		"""
		
		# =============  Step1N: Build 最大長度Grams ==================
		
		#加入 self.internalReplaceStr 到avoid字串中，因為層次取代過程中，self.internalReplaceStr是用來表示被取代後之字串
		if avoidStrs==None:
			avoidStrs=[self.internalReplaceStr]
		elif isinstance(avoidStrs, list):
			avoidStrs.add(avoidStrs)
		elif isinstance(avoidStrs, str):
			avoidStrs=[avoidStrs,self.internalReplaceStr]
			
		assert isinstance(avoidStrs,list)
		
		#由大到小建立，產生反向range-list
		for gramlen in range(maxGramLen,minGramLen,-1  ) :
			self.build_gram_proc_s1_gen_fixed_len_gram(gramlen,verbose,avoidStrs)
			
			# ========  Step2a: 計算文件數與門檻過濾(每輪執行)==================
			self.build_gram_proc_s2_docFilter_VLngram_byLayer(docThreshold,gramlen,verbose)  # VLn-gram 過濾 only
			
			# ======== 	Step2b: 重新文件內容.. 
			self.build_gram_proc_s2b_remove_FSgram_From_Text(docThreshold,gramlen,verbose) #清理過程仍需要重新過濾gram
			
		self.allgramsDict=self.featureSet								#用featureSet內容補回去
		#print(self.allgramsDict["婆羅"])
		
		# ================  Step3: 排序 =====================
		self.build_gram_proc_s3_sort(sortBy,verbose)
		#=============================================
